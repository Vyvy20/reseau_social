const Evenement = require('../models/evenementModel');

//Création nouveau évènement
exports.nouvelEvent = async (req, res) => {
  try {
    const nouvelEvent = new Evenement(req.body);
    const EventCree = await nouvelEvent.save();
    res.status(201).json(EventCree);
  } catch (erreur) {
    res.status(400).json({ message: erreur.message });
  }
};

//Récupérer des évènements
exports.recupEvents = async (req, res) => {
  try {
    const evenements = await Evenement.find();
    res.json(evenements);
  } catch (erreur) {
    res.status(500).json({ message: erreur.message });
  }
};

//Récupérer un évènement par son identifiant
exports.recupEventId = async (req, res) => {
  try {
    const evenement = await Evenement.findById(req.params.id);
    if (!evenement) {
      return res.status(404).json({ message: "Evènement introuvable" });
    }
    res.json(evenement);
  } catch (erreur) {
    res.status(500).json({ message: erreur.message });
  }
};

//Modifier un évènement existant
exports.modifEvent = async (req, res) => {
  try {
    const evenement = await Evenement.findByIdAndUpdate(req.params.id, req.body, { new: true });
    if (!evenement) {
      return res.status(404).json({ message: "Evènement introuvable" });
    }
    res.json(evenement);
  } catch (erreur) {
    res.status(400).json({ message: erreur.message });
  }
};

//Suppression d'un évènement existant
exports.suppEvenement = async (req, res) => {
  try {
    const event = await Evenement.findByIdAndDelete(req.params.id);
    if (!event) {
      return res.status(404).json({ message: "Evenement introuvable" });
    }
    res.json({ message: "Evenement supprimé avec succès" });
  } catch (erreur) {
    res.status(500).json({ message: erreur.message });
  }
};
