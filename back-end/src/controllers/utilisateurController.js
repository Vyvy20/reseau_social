const Utilisateur = require('../models/utilisateurModel');

//Création nouveau utilisateur
exports.nouveauUtilisateur = async (req, res) => {
  try {
    const nouvelUtilisateur = new Utilisateur(req.body);
    const utilisateurCree = await nouvelUtilisateur.save();
    res.status(201).json(utilisateurCree);
  } catch (erreur) {
    res.status(400).json({ message: erreur.message });
  }
};

//Récupérer des utilisateurs
exports.recupUtilisateurs = async (req, res) => {
  try {
    const utilisateurs = await Utilisateur.find();
    res.json(utilisateurs);
  } catch (erreur) {
    res.status(500).json({ message: erreur.message });
  }
};

//Récupérer un utilisateur par son identifiant
exports.recupUtilisateurId = async (req, res) => {
  try {
    const utilisateur = await Utilisateur.findById(req.params.id);
    if (!utilisateur) {
      return res.status(404).json({ message: "Utilisateur introuvable" });
    }
    res.json(utilisateur);
  } catch (erreur) {
    res.status(500).json({ message: erreur.message });
  }
};

//Modifier un utilisateur existant
exports.modifUtilisateur = async (req, res) => {
  try {
    const utilisateur = await Utilisateur.findByIdAndUpdate(req.params.id, req.body, { new: true });
    if (!utilisateur) {
      return res.status(404).json({ message: "Utilisateur introuvable" });
    }
    res.json(utilisateur);
  } catch (erreur) {
    res.status(400).json({ message: erreur.message });
  }
};

//Suppression d'un utilisateur existant
exports.suppUtilisateur = async (req, res) => {
  try {
    const utilisateur = await Utilisateur.findByIdAndDelete(req.params.id);
    if (!utilisateur) {
      return res.status(404).json({ message: "Utilisateur introuvable" });
    }
    res.json({ message: "Utilisateur supprimé avec succès" });
  } catch (erreur) {
    res.status(500).json({ message: erreur.message });
  }
};
