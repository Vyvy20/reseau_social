const mongoose = require('mongoose');

const evenementModelSchema = new mongoose.Schema(
    {
        nom: {
            type: String,
            required: true
        },
        description: {
            type: String,  // Correction ici
            required: true
        },
        adresseEvent: {
            numRue: {
                type: Number,
                required: true
            },
            nomRue: {
                type: String,
                required: true
            },
            codePostal: {
                type: String,
                required: true,
                validate: {
                    validator: function(codePostalProp) {
                        return codePostalProp.length <= 5;
                    },
                    message: props => `${props.value} doit contenir 4 caractères au maximum!`
                }
            },
            ville: {
                type: String,
                required: true,
            }
        },
        dateDebut: {
            type: Date,
            required: true,
        },
        dateFin: {
            type: Date,
            required: true,
        },
        statut: {
            type: Boolean,
            default: true,
        },
        imageEvent: {
            type: String,
            default: "https://carterie-fbdiffusion.fr/wp-content/uploads/469d.jpg"
        },
        participants: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'utilisateurModel'
        }],
        organisateurs: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'utilisateurModel'
        }]
    },
    {
        collection: "evenement",
        minimize: false,
        versionKey: false,
    }
).set('toJSON', {
    transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});

module.exports = mongoose.model('Evenement', evenementModelSchema);
