const mongoose = require('mongoose');

const utilisateurModelSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            unique: true,
            required: true,
        },
        nom: {
            type: String,
            required: true,
        },
        prenom: {
            type: String,
            required: true,
        },
        mdp: {
            type: String,
            required: true,
        },
        age: {
            type: Number,
            required: true,
        },
        adresse: {
            numRue: {
                type: Number,
                required: true
            },
            nomRue: {
                type: String,
                required: true
            },
            codePostal: {
                type: String,
                required: true,
                validate: {
                    validator: function(codePostalProp) {
                        return codePostalProp.length <= 5;
                    },
                    message: props => `${props.value} doit contenir 4 caractères au maximum!`
                }
            },
            ville: {
                type: String,
                required: true,
            }
        },
        numTel: {
            type: String,
            required: true,
            validate: {
                validator: function(numTellProp) {
                    return numTellProp.length <= 10;
                },
                message: props => `${props.value} doit contenir 4 caractères au maximum!`
            }
        },
        imageProfil: {
            type: String,
            default: "https://www.screenfeed.fr/wp-content/uploads/2013/10/default-avatar.png"
        }
    },
    {
        collection: "utilisateurs",
        minimize: false,
        versionKey: false,
    }
).set('toJSON', {
    transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});

module.exports = mongoose.model('Utilisateur', utilisateurModelSchema);

