const express = require('express');
const utilisateurController = require('../controllers/utilisateurController');
const eventController = require('../controllers/evenementController');
const Event = require('../models/evenementModel');

const router = express.Router();

//routes utilisateur
router.post('/utilisateurs', utilisateurController.nouveauUtilisateur);
router.get('/utilisateurs', utilisateurController.recupUtilisateurs);
router.get('/utilisateurs/:id', utilisateurController.recupUtilisateurId);
router.put('/utilisateurs/:id', utilisateurController.modifUtilisateur);
router.delete('/utilisateurs/:id', utilisateurController.suppUtilisateur);

//routes évènement
router.post('/evenements', eventController.nouvelEvent);
router.get('/evenements', eventController.recupEvents);
router.get('/evenements/:id', eventController.recupEventId);
router.put('/evenements/:id', eventController.modifEvent);
router.delete('/evenements/:id', eventController.suppEvenement);

//Lier utilisateur et évènements
router.get('/evenements/:id', async (req, res) => {
    try {
        const evenement = await Event.findById(req.params.id)
            .populate('participants')
            .populate('organisateurs');
        res.json(evenement);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

module.exports = router;