const express = require('express');
const mongoose = require('mongoose');
const Routes = require('./src/routes/Route');

const app = express();

app.use(express.json());

app.use('/api', Routes);

mongoose.connect('mongodb://localhost:27017/reseauSocial', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
.then(() => {
  console.log("Connexion à la base de données réussie");
})
.catch((err) => {
  console.error("Erreur de connexion à la base de données:", err);
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Serveur en écoute sur le port ${PORT}`);
});
